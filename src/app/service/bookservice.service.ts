import { Injectable } from '@angular/core';
import { Books } from '../model/Book';
import { books } from '../model/Bookslist';

@Injectable({
  providedIn: 'root'
})

export class BookserviceService {

  booklist: Books[];

  favlist: Books[];

  constructor() {
    this.booklist = books;
    this.favlist = [];
  }

  getBookById(id: number): any {
    return this.booklist.find(blog => blog.id = id);
  }

  addBook(book: Books) {
    console.log('add books');
    let id = this.booklist.length + 1;
    book.id = id;
    this.booklist.push(book);
    console.log(this.booklist);
  }

  getBooks() {
    console.log('get books');
    console.log(this.booklist);
    return this.booklist;
  }

  deleteBook(book: Books) {
    console.log('delete books');
    console.log(book);
    for (var j = 0; j < this.booklist.length; j++) {
      if (this.booklist[j] === book) {
        this.booklist.splice(j, 1);
        console.log("Book Deleted");
      }
    }
  }

  editBook(book: Books) {
    for (var j = 0; j < this.booklist.length; j++) {
      if (this.booklist[j].id === book.id) {
        this.booklist[j].title = book.title;
        this.booklist[j].author = book.author;
        this.booklist[j].year = book.year;
      }
    }
  }

  addLikedBooks(book: Books) {
    for (var j = 0; j < this.booklist.length; j++) {
      if (this.booklist[j].id === book.id) {
        this.favlist.push(this.booklist[j]);
        console.log(this.favlist);
      }
    }
    console.log(this.favlist);
  }

  getFavList(){
    this.favlist.forEach(a => {
      console.log(a);
    })
    
  }

  addReadLater(book: Books){
    for (var j = 0; j < this.booklist.length; j++) {
      if (this.booklist[j].id === book.id) {
        this.favlist.push(this.booklist[j]);
        console.log(this.favlist);
      }
    }
    console.log(this.favlist);
  }

  getReadLaterList(){
    this.favlist.forEach(a => {
      console.log(a);
    })
  }

}
