import { Component, Input, OnInit } from '@angular/core';
import { Books } from '../model/Book';

@Component({
  selector: 'app-booksdisplay',
  templateUrl: './booksdisplay.component.html',
  styleUrls: ['./booksdisplay.component.css']
})
export class BooksdisplayComponent implements OnInit {

  @Input()  
  blog: Books;

  constructor() {
    this.blog = new Books('','','');
  }

  ngOnInit(): void {
  }

}
