import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Books } from '../model/Book';
import { BookserviceService } from '../service/bookservice.service';

@Component({
  selector: 'app-bookblog',
  templateUrl: './bookblog.component.html',
  styleUrls: ['./bookblog.component.css']
})
export class BookblogComponent implements OnInit {

  books:Books[];

  constructor(private bs: BookserviceService,private router:Router) { 
    this.books = [];
  }

  ngOnInit(): void {
    this.books = this.bs.getBooks();
    console.log(this.books);
  }

  delete(books:Books){
    this.bs.deleteBook(books);
  }

  edit(books:Books){
    this.router.navigate(['/edit', books.id]);
  }

  addLiked(books:Books){
    this.bs.addLikedBooks(books);
    console.log(books);
    this.router.navigate(['favlist']);
  }

  readLater(books:Books){
    this.bs.addReadLater(books);
    console.log(books);
    this.router.navigate(['readlater']);
  }

}
