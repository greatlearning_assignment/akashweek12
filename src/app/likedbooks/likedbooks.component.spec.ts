import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LikedbooksComponent } from './likedbooks.component';

describe('LikedbooksComponent', () => {
  let component: LikedbooksComponent;
  let fixture: ComponentFixture<LikedbooksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LikedbooksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LikedbooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
