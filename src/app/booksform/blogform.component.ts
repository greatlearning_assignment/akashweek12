import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Books } from '../model/Book';
import { BookserviceService } from '../service/bookservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blogform',
  templateUrl: './blogform.component.html',
  styleUrls: ['./blogform.component.css']
})
export class BlogformComponent implements OnInit {

  blog:Books;
  
  constructor(private bs:BookserviceService,
    private router:Router) { 
    this.blog = new Books('','','');
  }

  onSubmit()
  {
    this.bs.addBook(this.blog);
    this.router.navigate(['/list']);
  }

  ngOnInit(): void {
  }
 
}
