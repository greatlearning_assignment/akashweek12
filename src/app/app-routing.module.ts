import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookblogComponent } from './bookblog/bookblog.component';
import { BlogeditComponent } from './bookedit/blogedit.component';
import { BlogformComponent } from './booksform/blogform.component';
import { LikedbooksComponent } from './likedbooks/likedbooks.component';
import { LoginComponent } from './login/login.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ReadlaterbooksComponent } from './readlaterbooks/readlaterbooks.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path:'',redirectTo:'bookslist',pathMatch:'full'},
  {path:'bookslist',component:BookblogComponent},
  {path:'edit/:id',component:BlogeditComponent},
  {path:'create',component:BlogformComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'favlist',component:LikedbooksComponent},
  {path:'readlater',component:ReadlaterbooksComponent},
  {path:'**',component:PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
